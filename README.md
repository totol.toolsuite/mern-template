mern-template

# Run dev server
```
yarn install-client-server
yarn dev
```

- The react project is exposed at the route / of the nodejs api
- If you want to develop with the React on port 3000 and nodejs on port 5000,
it is possible, just run dev server. 
- The react app app use the port 5000 as a fallback port in development, with the proxy line in the package.json.
So if a route isn''t found at port 3000, it will be searched at port 5000.
- Need a mongo atlas db to work with database