import React from "react";
import "./App.css";
import Admin from "./admin/Main";
import { Home } from "./layout/Home";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { Login } from "./layout/Login";
import { Register } from "./layout/Register";
import { Header } from "./component/Header";
import Exercises from "./layout/Exercises";
import { Provider } from "react-redux";
import { createHashHistory } from "history";
import restProvider from "./admin/providers/dataProvider";
import tokenProvider from "./admin/providers/authProvider";
import storeProvider from "./store";
import { ConnectedRouter } from "connected-react-router";

const dataProvider = restProvider("/api");
const authProvider = tokenProvider;
const history = createHashHistory();

const App = () => {
	const routes = [
		{
			path: "/exercises",
			component: <Exercises />,
		},
		{
			path: "/admin",
			component: (
				<Admin
					authProvider={authProvider}
					dataProvider={dataProvider}
					history={history}
					title="Administration"
				/>
			),
		},
		{
			path: "/register",
			component: <Register />,
		},
		{
			path: "/login",
			component: <Login />,
		},
		{
			path: "/",
			component: <Home />,
		},
	];

	return (
		<>
			<div className="App">
				<Provider
					store={storeProvider({
						authProvider,
						dataProvider,
						history,
					})}
				>
					<ConnectedRouter history={history}>
						<Route>
							<Header />
							<Switch>
								{routes.map((x, idx) => (
									<Route exact key={idx} path={x.path}>
										{x.component}
									</Route>
								))}
							</Switch>
						</Route>
					</ConnectedRouter>
				</Provider>
			</div>
		</>
	);
};

export default App;
