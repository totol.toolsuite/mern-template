import { Admin, Resource } from "react-admin";
import {
	ExerciseCreate,
	ExerciseEdit,
	ExerciseList,
} from "./resources/ExerciseAdmin";
import { UserCreate, UserEdit, UserList } from "./resources/UserAdmin";
import MyLayout from "./layouts/MyLayout";
import MyLogin from "./layouts/MyLogin";
import { useGetLoggedUser } from "../hooks/useGetLoggedUser";
import { Redirect, Route } from "react-router";

const AdminRoot = (props) => {
	const user = useGetLoggedUser();
	return user?.result?.role === "ADMIN" ? (
		<Route path="/admin">
			<Admin {...props} loginPage={MyLogin} layout={MyLayout}>
				<Resource
					name="exercises"
					list={ExerciseList}
					create={ExerciseCreate}
					edit={ExerciseEdit}
				></Resource>
				<Resource
					name="users"
					list={UserList}
					create={UserCreate}
					edit={UserEdit}
				></Resource>
			</Admin>
		</Route>
	) : (
		<Redirect to="/" />
	);
};

export default AdminRoot;
