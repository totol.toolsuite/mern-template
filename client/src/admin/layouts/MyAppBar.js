// MyAppBar.js

import React from "react";
import { AppBar } from "react-admin";
import Typography from "@material-ui/core/Typography";
import { withStyles } from "@material-ui/core/styles";
import IconButton from "@material-ui/core/IconButton";
import BackIcon from "@material-ui/icons/ArrowBack";
import { Link } from "react-router-dom";

const styles = {
	title: {
		flex: 1,
		textOverflow: "ellipsis",
		whiteSpace: "nowrap",
		overflow: "hidden",
	},
	spacer: {
		flex: 1,
	},
	link: {
		textDecoration: "none",
		color: "white",
	},
};
const MyAppBar = withStyles(styles)(({ classes, ...props }) => {
	return (
		<AppBar {...props}>
			<Typography className={classes.title} id="react-admin-title" />
			<span className={classes.spacer} />
			<Link className={classes.link} to="/">
				<IconButton
					className={classes.link}
					onClick={() => {
						window.history.replaceState({}, document.title, ".");
					}}
				>
					<BackIcon />
				</IconButton>
			</Link>
		</AppBar>
	);
});

export default MyAppBar;
