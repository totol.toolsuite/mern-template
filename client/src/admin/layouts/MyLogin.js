import React, { useState } from "react";
import { connect } from "react-redux";
import { userLogin } from "react-admin";
import { Box, Button, Card, Container, Grid } from "@material-ui/core";
import Input from "../../component/Input";

export const MyLoginPage = (props) => {
	const initialFormData = {
		email: "",
		password: "",
	};
	const [showPassword, setShowPassword] = useState(false);
	const [formData, setFormData] = useState(initialFormData);

	const handleShowPassword = () =>
		setShowPassword((prevShowPassword) => !prevShowPassword);

	const handleSubmit = (e) => {
		e.preventDefault();
		props.userLogin(formData);
	};
	const handleChange = (e) => {
		setFormData({ ...formData, [e.target.name]: e.target.value });
	};

	return (
		<Container>
			<Box m={4}>
				<Card elevation={5}>
					<form onSubmit={(e) => handleSubmit(e)}>
						<Grid container>
							<Input
								name="email"
								label="Email Adresse"
								handleChange={handleChange}
								type="email"
							/>
							<Input
								name="password"
								label="Password"
								handleChange={handleChange}
								type={showPassword ? "text" : "password"}
								handleShowPassword={handleShowPassword}
							/>
							<Button
								type="submit"
								fullWidth
								variant="contained"
								color="primary"
							>
								Login
							</Button>
						</Grid>
					</form>
				</Card>
			</Box>
		</Container>
	);
};

export default connect(undefined, { userLogin })(MyLoginPage);
