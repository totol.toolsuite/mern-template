// in dataProvider.js
import { fetchUtils } from "ra-core";
import inMemoryJWT from "../../utils/inMemoryJwt";

export default (apiUrl) => {
	const httpClient = (url) => {
		const options = {
			headers: new Headers({ Accept: "application/json" }),
		};
		const token = inMemoryJWT.getToken();
		if (token) {
			options.headers.set("Authorization", `Bearer ${token}`);
		}

		return fetchUtils.fetchJson(url, options);
	};

	return {
		getList: (resource, params) =>
			httpClient(`${apiUrl}/${resource}`).then(({ headers, json }) => ({
				data: json.data,
				total: json.total,
			})),
		getOne: (resource, params) =>
			httpClient(`${apiUrl}/${resource}/${params.id}`).then(({ json }) => ({
				data: json,
			})),
		getMany: () => Promise.reject(),
		getManyReference: () => Promise.reject(),
		update: (resource, params) =>
			httpClient(`${apiUrl}/${resource}/${params.id}`, {
				method: "PUT",
				body: JSON.stringify(params.data),
			}).then(({ json }) => ({ data: json })),
		updateMany: () => Promise.reject(),
		create: (resource, params) =>
			httpClient(`${apiUrl}/${resource}`, {
				method: "POST",
				body: JSON.stringify(params.data),
			}).then(({ json }) => ({
				data: { ...params.data, id: json.id },
			})),
		delete: (resource, params) =>
			httpClient(`${apiUrl}/${resource}/${params.id}`, {
				method: "DELETE",
				headers: new Headers({
					"Content-Type": "text/plain",
				}),
			}).then(({ json }) => ({ data: json })),
		deleteMany: () => Promise.reject(),
	};
};
