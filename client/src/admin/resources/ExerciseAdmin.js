import React from "react";
import {
  Create,
  Edit,
  SimpleForm,
  TextInput,
  DateInput,
  List,
  Datagrid,
  TextField,
  DateField,
  EditButton,
  DeleteButton,
} from "react-admin";

export const ExerciseCreate = (props) => {
  return (
    <Create title="Create an exercise" {...props}>
      <SimpleForm>
        <TextInput source="username" />
        <TextInput multiline source="description" />
        <TextInput source="duration" />
        <DateInput source="date" />
      </SimpleForm>
    </Create>
  );
};

export const ExerciseEdit = (props) => {
  return (
    <Edit title="Edit exercise" {...props}>
      <SimpleForm>
        <TextInput disabled source="id" />
        <TextInput source="username" />
        <TextInput multiline source="description" />
        <TextInput source="duration" />
        <DateInput source="date" />
      </SimpleForm>
    </Edit>
  );
};

export const ExerciseList = (props) => {
  return (
    <List {...props}>
      <Datagrid>
        <TextField source="_id" />
        <TextField source="username" />
        <TextField source="description" />
        <TextField source="duration" />
        <DateField source="date" />
        <EditButton basePath="/exercises" />
        <DeleteButton basePath="/exercises" />
      </Datagrid>
    </List>
  );
};
