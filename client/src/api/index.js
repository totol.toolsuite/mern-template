import axios from "axios";
import inMemoryJwt from "../utils/inMemoryJwt";

const baseUrl = "/api";

const API = axios.create({ baseUrl: "/api" });
API.interceptors.request.use((req) => {
	const token = inMemoryJwt.getToken();
	req.headers.Authorization = token;
	return req;
});
export const fetchExercises = () => API.get(`${baseUrl}/exercises`);
export const register = (formData) =>
	API.post(`${baseUrl}/auth/register`, formData);
export const login = (formData) => API.post(`${baseUrl}/auth/login`, formData);
