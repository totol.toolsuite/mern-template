import {
	AppBar,
	Box,
	Button,
	makeStyles,
	Toolbar,
	Typography,
} from "@material-ui/core";
import React from "react";
import { useDispatch } from "react-redux";
import { Link, useHistory, useLocation } from "react-router-dom";
import { useGetLoggedUser } from "../hooks/useGetLoggedUser";
import { LOGOUT } from "../store/actions/types";
const useStyles = makeStyles((theme) => ({
	root: {
		flexGrow: 1,
	},
	menuButton: {
		marginRight: theme.spacing(2),
	},
	menuBox: {
		width: "100%",
	},
	menuLink: {
		textDecoration: "none",
		color: "white",
		marginLeft: theme.spacing(2),
		marginRight: theme.spacing(2),
	},
}));

export const Header = () => {
	const classes = useStyles();
	const dispatch = useDispatch();
	const history = useHistory();
	const location = useLocation();
	const user = useGetLoggedUser(location);

	const logout = () => {
		dispatch({ type: LOGOUT });
		history.push("/login");
	};

	return (
		<AppBar position="relative">
			<Toolbar>
				<Typography variant="h6" color="inherit">
					Application
				</Typography>
				<Box
					display="flex"
					flexDirection="row"
					justifyContent="flex-end"
					alignItems="center"
					className={classes.menuBox}
					spacing={3}
				>
					<Typography variant="h6" className={classes.menuLink}>
						{user?.result &&
							`${user?.result?.firstName} - ${user?.result?.lastName}`}
					</Typography>
					<Link className={classes.menuLink} to="/exercises">
						Exercises
					</Link>
					<Link className={classes.menuLink} to="/">
						Home
					</Link>
					<Link className={classes.menuLink} to="/login">
						Login
					</Link>
					<Link className={classes.menuLink} to="/register">
						Register
					</Link>
					{user?.result?.role === "ADMIN" && (
						<Link className={classes.menuLink} to="/admin">
							Admin
						</Link>
					)}
				</Box>
				{user?.result && (
					<Button variant="outlined" color="secondary" onClick={() => logout()}>
						Logout
					</Button>
				)}
			</Toolbar>
		</AppBar>
	);
};
