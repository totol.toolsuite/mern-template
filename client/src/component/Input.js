import {
	Grid,
	IconButton,
	InputAdornment,
	makeStyles,
	TextField,
} from "@material-ui/core";
import { Visibility, VisibilityOff } from "@material-ui/icons";
import React from "react";

const useStyles = makeStyles((theme) => ({
	container: {
		margin: theme.spacing(1),
		padding: theme.spacing(2),
	},
}));

const Input = ({
	name,
	handleChange,
	label,
	half,
	autoFocus,
	type,
	handleShowPassword,
	required,
}) => {
	const classes = useStyles();

	return (
		<Grid item xs={12} sm={half ? 5 : 12} className={classes.container}>
			<TextField
				name={name}
				onChange={handleChange}
				variant="outlined"
				required={required}
				fullWidth
				label={label}
				autoFocus={autoFocus}
				autoComplete="on"
				type={type}
				InputProps={
					name === "password"
						? {
								endAdornment: (
									<InputAdornment position="end">
										<IconButton onClick={() => handleShowPassword()}>
											{type === "password" ? <Visibility /> : <VisibilityOff />}
										</IconButton>
									</InputAdornment>
								),
						  }
						: {}
				}
			></TextField>
		</Grid>
	);
};

export default Input;
