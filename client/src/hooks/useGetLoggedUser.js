import { useState, useMemo, useEffect } from "react";
export function useGetLoggedUser(location) {
	const getProfile = useMemo(() => {
		try {
			return JSON.parse(localStorage.getItem("profile"));
		} catch (error) {
			return null;
		}
	}, [location]);

	const [user, setUser] = useState(getProfile);

	useEffect(() => {
		setUser(getProfile);
	}, [location]);

	return user;
}
