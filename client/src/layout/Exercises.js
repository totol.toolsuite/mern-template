import { Container } from "@material-ui/core";
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getExercises } from "../store/actions/exercises";

const Exercises = () => {
	const dispatch = useDispatch();
	const exercises = useSelector((state) => state.exercises?.exercises);

	useEffect(() => {
		dispatch(getExercises());
	}, [dispatch]);

	return (
		<Container>
			{exercises?.data.map((x) => (
				<p key={x._id}>{x.description}</p>
			))}
		</Container>
	);
};
export default Exercises;
