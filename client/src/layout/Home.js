import React, { useState } from "react";

export const Home = () => {
	const [msg, setMsg] = useState("");
	const handleClick = async () => {
		const data = await window.fetch("/api/test");
		const json = await data.json();
		setMsg(json.msg);
	};

	return (
		<div>
			<button onClick={() => handleClick()}>Test api</button>
			<p>{msg}</p>
		</div>
	);
};
