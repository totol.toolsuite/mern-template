import { Box, Button, Card, Container, Grid } from "@material-ui/core";
import React, { useState } from "react";
import { useDispatch } from "react-redux";
import Input from "../component/Input";
import { login } from "../store/actions/auth";
import { useHistory } from "react-router-dom";

export const Login = () => {
	const initialFormData = {
		email: "",
		password: "",
	};
	const [showPassword, setShowPassword] = useState(false);
	const [formData, setFormData] = useState(initialFormData);
	const dispatch = useDispatch();
	const history = useHistory();

	const handleShowPassword = () =>
		setShowPassword((prevShowPassword) => !prevShowPassword);

	const handleSubmit = (e) => {
		e.preventDefault();
		dispatch(login(formData, history));
	};
	const handleChange = (e) => {
		setFormData({ ...formData, [e.target.name]: e.target.value });
	};

	return (
		<Container>
			<Box m={4}>
				<Card elevation={5}>
					<form onSubmit={(e) => handleSubmit(e)}>
						<Grid container>
							<Input
								name="email"
								label="Email Adresse"
								handleChange={handleChange}
								type="email"
							/>
							<Input
								name="password"
								label="Password"
								handleChange={handleChange}
								type={showPassword ? "text" : "password"}
								handleShowPassword={handleShowPassword}
							/>
							<Button
								type="submit"
								fullWidth
								variant="contained"
								color="primary"
							>
								Login
							</Button>
						</Grid>
					</form>
				</Card>
			</Box>
		</Container>
	);
};
