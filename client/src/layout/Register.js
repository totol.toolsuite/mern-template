import { Box, Button, Card, Container, Grid } from "@material-ui/core";
import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { useHistory } from "react-router-dom";
import Input from "../component/Input";
import { register } from "../store/actions/auth";

export const Register = () => {
	const initialFormData = {
		firstName: "",
		lastName: "",
		email: "",
		password: "",
		confirmPassword: "",
	};
	const [showPassword, setShowPassword] = useState(false);
	const [formData, setFormData] = useState(initialFormData);
	const dispatch = useDispatch();
	const history = useHistory();
	const handleShowPassword = () =>
		setShowPassword((prevShowPassword) => !prevShowPassword);

	const handleSubmit = (e) => {
		e.preventDefault();
		dispatch(register(formData, history));
	};
	const handleChange = (e) => {
		setFormData({ ...formData, [e.target.name]: e.target.value });
	};

	return (
		<Container>
			<Box m={4}>
				<Card elevation={5}>
					<form onSubmit={(e) => handleSubmit(e)}>
						<Grid container>
							<Input
								name="firstName"
								label="First Name"
								handleChange={handleChange}
								autoFocus
								half
							/>
							<Input
								name="lastName"
								label="Last Name"
								handleChange={handleChange}
								half
							/>
							<Input
								name="email"
								label="Email Adresse"
								handleChange={handleChange}
								type="email"
							/>
							<Input
								name="password"
								label="Password"
								handleChange={handleChange}
								type={showPassword ? "text" : "password"}
								handleShowPassword={handleShowPassword}
							/>
							<Input
								name="confirmPassword"
								label="Repeat password"
								handleChange={handleChange}
								type={showPassword ? "text" : "password"}
								handleShowPassword={handleShowPassword}
							/>
							<Button
								type="submit"
								fullWidth
								variant="contained"
								color="primary"
							>
								Register
							</Button>
						</Grid>
					</form>
				</Card>
			</Box>
		</Container>
	);
};
