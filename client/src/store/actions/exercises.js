import * as api from "../../api";
import { EXERCISES_FETCH_ALL } from "./types";
//Action Creators
export const getExercises = () => async (dispatch) => {
	try {
		const { data } = await api.fetchExercises();
		dispatch({ type: EXERCISES_FETCH_ALL, payload: data });
	} catch (error) {
		console.log(error.message);
	}
};
