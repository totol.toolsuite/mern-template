export const EXERCISES_FETCH_ALL = "EXERCISES_FETCH_ALL";
export const EXERCISES_CREATE = "EXERCISES_CREATE";
export const AUTH = "AUTH";
export const LOGOUT = "LOGOUT";
export const REGISTER = "REGISTER";
export const LOGIN = "LOGIN";
