import { AUTH, LOGOUT } from "../actions/types.js";

const initialState = {
	authData: null,
};

const reducer = (state = initialState, action) => {
	switch (action.type) {
		case AUTH:
			localStorage.setItem("profile", JSON.stringify({ ...action?.data }));
			return { ...state, authData: action?.data };
		case LOGOUT:
			localStorage.setItem("profile", "");
			return { ...state, authData: null };
		default:
			return state;
	}
};

export default reducer;
