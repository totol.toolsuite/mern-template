import { EXERCISES_FETCH_ALL, EXERCISES_CREATE } from "../actions/types.js";

const initialState = {
	exercises: [],
};

const reducer = (state = initialState, action) => {
	switch (action.type) {
		case EXERCISES_FETCH_ALL:
			return { ...state, exercises: action.payload };
		case EXERCISES_CREATE:
			return state;
		default:
			return state;
	}
};

export default reducer;
