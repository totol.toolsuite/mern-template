import mongoose from "mongoose";
import User from "../models/user.model.js";
import bcrypt from "bcryptjs";
import jwt from "jsonwebtoken";

export const login = async (req, res) => {
	const { email, password } = req.body;
	try {
		const existingUser = await User.findOne({ email });
		if (!existingUser) {
			return res.status(404).json({ message: "User doesn't exist" });
		}
		const isPasswordCorrect = await bcrypt.compare(
			password,
			existingUser.password
		);
		if (!isPasswordCorrect) {
			return res.status(400).json({ message: "Invalid credentials" });
		}

		const token = jwt.sign(
			{
				email: existingUser.email,
				id: existingUser._id,
				role: existingUser.role,
			},
			process.env.SECRET_TOKEN_STRING,
			{ expiresIn: "1h" }
		);
		res.status(200).json({ result: existingUser, token });
	} catch (error) {
		res.status(500).json({ message: "Something went wrong" });
	}
};

export const register = async (req, res) => {
	const { firstName, lastName, email, password, confirmPassword } = req.body;
	try {
		const existingUser = await User.findOne({ email });
		if (existingUser) {
			return res
				.status(400)
				.json({ message: "User already exist with this email" });
		}
		if (password !== confirmPassword) {
			return res
				.status(400)
				.json({ message: "Password and confirm are differents" });
		}

		const hashedPassword = await bcrypt.hash(password, 12);
		const result = await User.create({
			email,
			password: hashedPassword,
			firstName,
			lastName,
		});
		const token = jwt.sign(
			{
				email: result.email,
				id: result._id,
				role: result.role,
			},
			process.env.SECRET_TOKEN_STRING,
			{ expiresIn: "1h" }
		);
		res.status(200).json({ result, token });
	} catch (error) {
		res.status(500).json({ message: "Something went wrong" });
	}
};
