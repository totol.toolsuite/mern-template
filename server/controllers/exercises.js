import mongoose from "mongoose";
import Exercise from "../models/exercise.model.js";

export const getExercises = async (req, res) => {
	try {
		const exercises = await Exercise.find();
		res.status(200).json({ data: exercises, total: exercises.length });
	} catch (error) {
		res.status(404).json({ message: error.message });
	}
};

export const getExercise = async (req, res) => {
	const { id } = req.params;
	try {
		const post = await Exercise.findById(id);
		res.status(200).json(post);
	} catch (error) {
		res.status(404).json({ message: error.message });
	}
};

export const createExercise = async (req, res) => {
	const { username, description, duration, date } = req.body;
	const newExercise = new Exercise({ username, description, duration, date });
	try {
		await newExercise.save();

		res.status(201).json(newExercise);
	} catch (error) {
		res.status(409).json({ message: error.message });
	}
};

export const updateExercise = async (req, res) => {
	const { id } = req.params;
	const { username, description, duration, date } = req.body;
	if (!mongoose.Types.ObjectId.isValid(id))
		return res.status(404).send(`No exercise with id: ${id}`);

	const updatedExercise = { username, description, duration, date, id: id };

	await Exercise.findByIdAndUpdate(id, updatedExercise, { new: true });

	res.json(updatedExercise);
};

export const deleteExercise = async (req, res) => {
	const { id } = req.params;

	if (!mongoose.Types.ObjectId.isValid(id))
		return res.status(404).send(`No exercise with id: ${id}`);

	await Exercise.findByIdAndRemove(id);

	res.json({ message: "Exercise deleted successfully." });
};
