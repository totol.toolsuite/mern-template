import mongoose from "mongoose";
import User from "../models/user.model.js";
import bcrypt from "bcryptjs";
export const getUsers = async (req, res) => {
	try {
		const users = await User.find();
		res.status(200).json({ data: users, total: users.length });
	} catch (error) {
		res.status(404).json({ message: error.message });
	}
};

export const getUser = async (req, res) => {
	const { id } = req.params;
	try {
		const post = await User.findById(id);
		res.status(200).json(post);
	} catch (error) {
		res.status(404).json({ message: error.message });
	}
};

export const createUser = async (req, res) => {
	const { firstName, lastName, email, password, role } = req.body;
	try {
		const hashedPassword = await bcrypt.hash(password, 12);

		const result = await User.create({
			email,
			password: hashedPassword,
			firstName,
			lastName,
			role,
		});
		res.json(result);
	} catch (error) {
		res.status(409).json({ message: error.message });
	}
};

export const updateUser = async (req, res) => {
	const { id } = req.params;
	if (!mongoose.Types.ObjectId.isValid(id))
		return res.status(404).send(`No user with id: ${id}`);
	const { firstName, lastName, email, password, role } = req.body;
	try {
		const hashedPassword = await bcrypt.hash(password, 12);
		const updatedUser = {
			firstName,
			lastName,
			email,
			password: hashedPassword,
			role,
			id: id,
		};

		await User.findByIdAndUpdate(id, updatedUser, { new: true });

		res.json(updatedUser);
	} catch (error) {
		res.status(419).json({ message: error.message });
	}
};

export const deleteUser = async (req, res) => {
	const { id } = req.params;

	if (!mongoose.Types.ObjectId.isValid(id))
		return res.status(404).send(`No user with id: ${id}`);

	await User.findByIdAndRemove(id);

	res.json({ message: "User deleted successfully." });
};
