import jwt from "jsonwebtoken";
import { clog } from "../utils/log.js";

export const ROLES_CONST = {
	ADMIN: "ADMIN",
	BASIC: "BASIC",
};

export const ROLES = [ROLES_CONST.ADMIN, ROLES_CONST.BASIC];

export const isLogged = async (req, res, next) => {
	try {
		const token = req.headers.authorization?.split(" ")[1];
		const decodedData = jwt.verify(token, process.env.SECRET_TOKEN_STRING);
		if (decodedData?.id) {
			next();
		} else {
			res.status(403).json({ message: "Not logged" });
		}
	} catch (error) {
		clog("error", "AUTH-MIDDLEWARE", "Something broken in auth");
	}
};

export const isAdmin = async (req, res, next) => {
	try {
		const token = req.headers?.authorization?.split(" ")[1];
		if (token) {
			const decodedData = jwt.verify(token, process.env.SECRET_TOKEN_STRING);
			if (decodedData?.role === "ADMIN") {
				next();
			} else {
				clog("error", "AUTH-MIDDLEWARE", JSON.stringify(decodedData));
				res.status(403).json({ message: "Permission denied" });
			}
		} else {
			res.status(401).json({ message: "Need to be logged" });
		}
	} catch (error) {
		clog("error", "AUTH-MIDDLEWARE", "Something broken in auth");
	}
};
