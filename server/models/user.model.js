import mongoose from "mongoose";
import { ROLES, ROLES_CONST } from "../middlewares/roles.js";
const { Schema } = mongoose;

const userSchema = new Schema(
	{
		firstName: {
			type: String,
			required: false,
			trim: true,
			minlength: 3,
		},
		lastName: {
			type: String,
			required: false,
			trim: true,
			minlength: 3,
		},
		email: {
			type: String,
			required: true,
			unique: true,
		},
		password: {
			type: String,
			required: true,
		},
		role: {
			type: String,
			enum: [...ROLES],
			default: ROLES_CONST.BASIC,
		},
	},
	{
		timestamps: true,
	}
);

userSchema.virtual("id").get(function () {
	return this._id.toHexString();
});

// Ensure virtual fields are serialised.
userSchema.set("toJSON", {
	virtuals: true,
});

export default mongoose.model("User", userSchema);
