import express from "express";

import {
  getExercises,
  getExercise,
  createExercise,
  updateExercise,
  deleteExercise,
} from "../controllers/exercises.js";

const router = express.Router();

router.get("/", getExercises);
router.post("/", createExercise);
router.get("/:id", getExercise);
router.put("/:id", updateExercise);
router.delete("/:id", deleteExercise);

export default router;
