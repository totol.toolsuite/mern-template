import express from "express";
import { clog } from "../utils/log.js";

const router = express.Router();

router.route("/").get((req, res) => {
  clog("response", "API | Response", "Server up");

  res.send({
    msg: "Helloo",
  });
});

export default router;
