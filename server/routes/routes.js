import express from "express";
import usersRouter from "./users.js";
import authRouter from "./auth.js";
import exercisesRouter from "./exercises.js";
import pingRouter from "./ping.js";

import { isAdmin, isLogged } from "../middlewares/roles.js";

const router = express.Router();

router.use("/exercises", isLogged, exercisesRouter);
router.use("/users", isAdmin, usersRouter);
router.use("/auth", authRouter);
router.use("/test", pingRouter);

export default router;
