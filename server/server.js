import express from "express";
import cors from "cors";
import mongoose from "mongoose";
import dotenv from "dotenv";
import path from "path";
import mainRouter from "./routes/routes.js";
import { clog } from "./utils/log.js";
dotenv.config({ path: `${path.resolve()}/./../.env` });

const PORT = process.env.PORT || 5000;
const URI = process.env.ATLAS_URI;

const app = express();

app.use(cors());
app.use(express.json());
app.use(express.static("../client/build"));
app.use((req, res, next) => {
  res.setHeader("Access-Control-Expose-Headers", "Content-Range");
  res.setHeader("Content-Range", "posts 0-20/20");
  clog("request", "API | REQUEST", req.originalUrl);
  next();
});
app.use("/api", mainRouter);

app.get("/*", (_, res) => {
  res.sendFile(path.join(path.resolve(), "../client/build/index.html"));
});

mongoose.connect(URI, {
  useNewUrlParser: true,
  useCreateIndex: true,
  useUnifiedTopology: true,
  useFindAndModify: false,
});

const connection = mongoose.connection;
connection.once("open", () => {
  clog(
    "success",
    "INIT",
    "MongoDB database connection established succesfully"
  );
});

app.listen(PORT, () =>
  clog("success", "INIT", `Server started on port ${PORT}`)
);
