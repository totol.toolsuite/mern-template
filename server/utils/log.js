import chalk from "chalk";

/**
 *
 * @param {(success|warning|error|req|res)} type
 * @param {*} prefix
 * @param {*} message
 */
export const clog = (type, prefix, message) => {
  switch (type) {
    case "success":
      {
        console.log(chalk.green(`[${prefix}] - ${message}`));
      }
      break;
    case "warning":
      {
        console.log(chalk.orange(`[${prefix}] - ${message}`));
      }
      break;
    case "error":
      {
        console.log(chalk.red(`[${prefix}] - ${message}`));
      }
      break;
    case "request":
      {
        console.log(chalk.yellow(`[${prefix}] - ${message}`));
      }
      break;
    case "response":
      {
        console.log(chalk.blue(`[${prefix}] - ${message}`));
      }
      break;
    default: {
      console.log(`[${prefix}] - ${message}`);
    }
  }
};
